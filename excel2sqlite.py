import sqlite3
import pandas


class ExcelSettings:
    file_path = ""

    # row/column indexed from 0 and includes the last value.
    # for example 4 rows of data could be start=2 and end=5
    # The data must be contiguous in excel for this to work, no skipping of rows and columns!
    row_start = 0       # first row of data
    row_end = 0         # Last row of data
    column_start = 0    # first column of data
    column_end = 0      # last column of data

    key_column_index = 0


class SqliteSettings:
    db_file_path = ""
    table_name = ""
    key_column_name = "id"
    excel2db_columns_map = ["id, column1, column2"]  # The names of your SQL columns, in order of the excel columns
    type_cast_map = [int, str, str]


def load_excel_data(file_name):
    """ Load all excel data from one sheet """
    excel_data_df = pandas.read_excel(file_name)
    # You can specify excel sheets here if needed, like this:
    # excel_data_df = pandas.read_excel('test/testdata.xlsx', sheet_name='Employees')

    # print whole sheet data
    print(excel_data_df)

    return excel_data_df


def update_sql_table(db, settings, data):
    """
    Write dictionary data to sqlite table.
    Expects a dictionary with dict key as the db keys value and all db column values in a list as dict value
    - check the test_update_sql_table for an example in test_excel2sqlite.py
    """

    for key_value in data.keys():
        for i in range(0, len(data[key_value])):

            # type cast:
            val = settings.type_cast_map[i](data[key_value][i])

            q = "UPDATE {} SET {}='{}' WHERE {}='{}'".format(
                settings.table_name,
                settings.excel2db_columns_map[i],
                val,
                settings.key_column_name,
                key_value
            )

            print(q)
            db.execute(q)


def transfer(excel_settings, db_settings):
    """ transfer excel data to sqlite """

    excel_data_df = load_excel_data(excel_settings.file_path)

    db_data = dict()

    for row in range(excel_settings.row_start, excel_settings.row_end + 1):
        # print(excel_data_df.iloc[row][key_index])
        key = excel_data_df.iloc[row][excel_settings.key_column_index]

        # hardcode key as integer string:
        key = str(int(key))

        row_data = list()
        for column in range(excel_settings.column_start, excel_settings.column_end + 1):
            # print(excel_data_df.iloc[row][column])
            val = excel_data_df.iloc[row][column]
            row_data.append(val)
        db_data[key] = row_data.copy()

    db = sqlite3.connect(db_settings.db_file_path)
    print("db_data:", db_data)
    update_sql_table(db, db_settings, db_data)
    db.commit()
    db.close()


if __name__ == "__main__":
    # Configure transfer to DB:
    excel_settings = ExcelSettings()
    db_settings = SqliteSettings()

    excel_settings.file_path = 'test/testdata.xlsx'
    excel_settings.row_start = 2
    excel_settings.row_end = 13
    excel_settings.column_start = 1
    excel_settings.column_end = 4
    excel_settings.key_column_index = 2

    db_settings.db_file_path = "testdb.sqlite3"
    db_settings.table_name = "test"
    db_settings.key_column_name = "c2"
    db_settings.excel2db_columns_map = ["c1", "c2", "c3", "c4"]
    db_settings.type_cast_map = [str, int, int, float]

    transfer(excel_settings, db_settings)
