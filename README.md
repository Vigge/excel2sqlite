# excel2sqlite
A script for updating sqlite3 DBs from an excel sheet

## Environment
Use python 3.7 or later

Required packages:
* pandoc
* xlrd

### install
1. If you need python (on a fresh windows install for instance) grab the official installer from the interwebs, pick the
latest python 3

2. [optional] set up a venv:
    
    On macOS and Linux:
    ```
    python3 -m venv env
    ```
    On Windows:
    ```
    py -m venv env
    ```

3. Install the required packages
    ``` 
    pip install pandoc
    pip install xlrd
    ```
    
    
## Running it
The only required file is "excel2sqlite.py". You will need to configure how the excel sheet should be read by modifying
the script. Modify the section below: 
```
if __name__ == "__main__":
```
Then run excel2sqlite.py

Usually on windows:
```
python excel2sqlite.py
```
or usually on linux:
```
python3 excel2sqlite.py
```
or if you have a venv:
```
<your-venv-dir>/bin/python excel2sqlite.py
```



## Run the tests
You can run the unit tests in test/test_excel2sqlite.py from an ide (pycharm for instance) or you can run them from
command line like this:

```
cd <your-repo-dir>/excel2sqlite/test
../venv/bin/python -m unittest test_excel2sqlite.Excel2Sqlite
# above assumes you installed your venv in the root of this repo
```
or if you don't use venv:
```
cd <your-repo-dir>/excel2sqlite/test
python3 -m unittest test_excel2sqlite.Excel2Sqlite
```
