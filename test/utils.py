import sqlite3
import pandas

''' test utilities '''


class TestUtils:

    def __init__(self):
        pass

    def create_fill_table(self, db, data, table_name):
        db.execute("CREATE TABLE {} (c1, c2, c3, c4)".format(table_name))

        for row_data in data:
            q = "INSERT INTO {} (c1, c2, c3, c4) ".format(table_name)
            q += "VALUES ('{}', '{}', '{}', '{}');".format(row_data[0], row_data[1], row_data[2], row_data[3])
            # print(q)
            db.execute(q)
            db.commit()

    def create_test_db(self, db_name):
        excel_data_df = pandas.read_excel("testdata.xlsx")

        data = list()
        for row in range(2, 14):
            # print(excel_data_df.iloc[row][key_index])
            # print("Entry from excel:")
            column_data = list()
            for column in range(1, 5):
                value = excel_data_df.iloc[row][column]
                if isinstance(value, float):
                    value = int(value)
                column_data.append(value)

            data.append(column_data)

        db = sqlite3.connect(db_name)
        self.create_fill_table(db, data, "test")

        db.close()
