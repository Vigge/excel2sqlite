import unittest
import sqlite3
import sys
import os
from utils import TestUtils

sys.path.append("..")
from excel2sqlite import transfer, load_excel_data, update_sql_table, SqliteSettings, ExcelSettings

DB_NAME = "testdb.sqlite3"

# The output that load_excel_data should produce when converted to str
EXCEL_VERF = """    Unnamed: 0     Unnamed: 1  Unnamed: 2  Unnamed: 3  Unnamed: 4
0          NaN            NaN         NaN         NaN         NaN
1          NaN            NaN         NaN         NaN         NaN
2          NaN        William         1.0        42.0         0.5
3          NaN  ClevelandJess         2.0        84.0         1.0
4          NaN          Lynch         3.0       126.0         1.5
5          NaN         Hoover         4.0       168.0         2.0
6          NaN        Beattie         5.0       210.0         2.5
7          NaN        Cordova         6.0       252.0         3.0
8          NaN      Dominguez         7.0       294.0         3.5
9          NaN         Matteo         8.0       336.0         4.0
10         NaN          Kiara         9.0       378.0         4.5
11         NaN        Bridget        10.0       420.0         5.0
12         NaN        Herbert        11.0       462.0         5.5
13         NaN         Marnie        12.0       504.0         6.0"""

class Excel2Sqlite(unittest.TestCase):

    def setUp(self):
        if os.path.isfile(DB_NAME):
            os.remove(DB_NAME)

        utils = TestUtils()
        utils.create_test_db(DB_NAME)

    # def tearDown(self):

    def test_xlsx(self):
        # transfer("testdata.xlsx", 2, 10, 1, 5, DB_NAME)
        data = load_excel_data("testdata.xlsx")
        self.assertEqual(str(data), EXCEL_VERF)

    def test_xlsm(self):
        # transfer("testdata.xlsx", 2, 10, 1, 5, DB_NAME)
        data = load_excel_data("testdata.xlsm")
        self.assertEqual(str(data), EXCEL_VERF)

    def test_xls(self):
        # transfer("testdata.xlsx", 2, 10, 1, 5, DB_NAME)
        data = load_excel_data("testdata.xls")
        self.assertEqual(str(data), EXCEL_VERF)

    # def test_csv(self):   # testdata seems to be in wrong format, -not supported
    #     data = load_excel_data("testdata.csv")
    #     self.assertEqual(str(data), EXCEL_VERF)
    #
    # def test_ods(self):   # not supported by pandoc
    #     data = load_excel_data("testdata.ods")
    #     self.assertEqual(str(data), EXCEL_VERF)

    def test_update_sql_table(self):
        db = sqlite3.connect(DB_NAME)

        settings = SqliteSettings()
        settings.table_name = "test"
        settings.key_column_name = "c2"
        settings.excel2db_columns_map = ["c1", "c2", "c3", "c4"]
        settings.type_cast_map = [str, int, int, float]

        data = {"1": ["William", 1, 45, 1], "2": ["Jess", 2, 84, 1]}

        update_sql_table(db, settings, data)

        self.assertEqual(db.execute("SELECT c3 from test WHERE c2=='1'").fetchone()[0], "45")
        self.assertEqual(db.execute("SELECT c1 from test WHERE c2=='2'").fetchone()[0], "Jess")
        db.close()

    def test_transfer(self):
        # Set all c4 fields to 0 so that we can test updating them from excel
        db = sqlite3.connect(DB_NAME)
        db.execute("UPDATE test SET c4='0'")
        db.close()

        # Configure transfer to DB:
        excel_settings = ExcelSettings()
        db_settings = SqliteSettings()

        excel_settings.file_path = "testdata.xlsx"
        excel_settings.row_start = 2
        excel_settings.row_end = 13
        excel_settings.column_start = 1
        excel_settings.column_end = 4
        excel_settings.key_column_index = 2

        db_settings.db_file_path = DB_NAME
        db_settings.table_name = "test"
        db_settings.key_column_name = "c2"
        db_settings.excel2db_columns_map = ["c1", "c2", "c3", "c4"]
        db_settings.type_cast_map = [str, int, int, float]

        transfer(excel_settings, db_settings)

        db = sqlite3.connect(DB_NAME)
        print(db.execute("SELECT * from test").fetchall())
        # William, 1 has c4 == 0.5
        self.assertEqual(db.execute("SELECT c4 from test WHERE c2=='1'").fetchone()[0], "0.5")
        # Cordova, 6 has c4 == 3.0
        self.assertEqual(db.execute("SELECT c4 from test WHERE c2=='6'").fetchone()[0], "3.0")
        # Marnie, 12 has c4 == 6.0
        self.assertEqual(db.execute("SELECT c4 from test WHERE c2=='12'").fetchone()[0], "6.0")
        db.close()

if __name__ == '__main__':
    unittest.main()
